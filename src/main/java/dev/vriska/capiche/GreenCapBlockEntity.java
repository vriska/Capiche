package dev.vriska.capiche;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class GreenCapBlockEntity extends BlockEntity {
    private int ticksRemaining = 0;

    public GreenCapBlockEntity(BlockPos pos, BlockState state) {
        super(Capiche.GREEN_CAP_BLOCK_ENTITY, pos, state);
    }

    @Override
    protected void writeNbt(NbtCompound nbt) {
        nbt.putInt("ticksRemaining", ticksRemaining);

        super.writeNbt(nbt);
    }

    @Override
    public void readNbt(NbtCompound nbt) {
        super.readNbt(nbt);

        ticksRemaining = nbt.getInt("ticksRemaining");
    }

    public static void tick(World world, BlockPos pos, BlockState state, GreenCapBlockEntity be) {
        if (state.get(GreenCapBlock.RECOVERING)) {
            if (be.ticksRemaining == 0) {
                be.ticksRemaining = 100;
            } else if (be.ticksRemaining > 0) {
                be.ticksRemaining -= 1;
            }

            if (be.ticksRemaining == 0) {
                world.setBlockState(pos, state.with(GreenCapBlock.RECOVERING, false));
            }
        }
    }
}
