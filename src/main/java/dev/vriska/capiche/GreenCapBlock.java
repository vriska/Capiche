package dev.vriska.capiche;

import net.minecraft.block.*;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.Entity;
import net.minecraft.entity.ItemEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.BooleanProperty;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

public class GreenCapBlock extends Block implements BlockEntityProvider {
    public static BooleanProperty RECOVERING = BooleanProperty.of("recovering");

    public GreenCapBlock(Settings settings) {
        super(settings);
        setDefaultState(getDefaultState().with(RECOVERING, false));
    }

    @Override
    protected void appendProperties(StateManager.Builder<Block, BlockState> builder) {
        builder.add(RECOVERING);
    }

    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new GreenCapBlockEntity(pos, state);
    }

    @Override
    public VoxelShape getCollisionShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context) {
        if (state.contains(RECOVERING) && state.get(RECOVERING)) return VoxelShapes.empty();

        if (context instanceof EntityShapeContext entityShapeContext) {
            Entity entity = entityShapeContext.getEntity();
            if (entity != null) {
                if (!context.isAbove(VoxelShapes.fullCube(), pos, true) && entity.getVelocity().y > 0) {
                    return VoxelShapes.empty();
                }
            }
        }

        return VoxelShapes.fullCube();
    }

    @Override
    public void onEntityCollision(BlockState state, World world, BlockPos pos, Entity entity) {
        super.onEntityCollision(state, world, pos, entity);

        if (!state.get(RECOVERING)) {
            world.setBlockState(pos, state.with(RECOVERING, true));

            ItemStack stack = new ItemStack(Capiche.METAL_CAP);
            ItemEntity item = new ItemEntity(world, pos.getX() + 0.5f, pos.getY(), pos.getZ() + 0.5f, stack);
            item.setPickupDelay(30);
            world.spawnEntity(item);
        }
    }

    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        if (type.equals(Capiche.GREEN_CAP_BLOCK_ENTITY)) {
            return (world1, pos, state1, blockEntity) -> GreenCapBlockEntity.tick(world1, pos, state1, (GreenCapBlockEntity) blockEntity);
        } else {
            return null;
        }
    }
}
