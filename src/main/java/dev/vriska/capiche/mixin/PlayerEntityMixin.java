package dev.vriska.capiche.mixin;

import dev.vriska.capiche.Capiche;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityPose;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.mob.HostileEntity;
import net.minecraft.entity.mob.Monster;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.registry.tag.DamageTypeTags;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(PlayerEntity.class)
public class PlayerEntityMixin {
    @Inject(at = @At("HEAD"), method = "isInvulnerableTo", cancellable = true)
    public void isInvulnerableTo(DamageSource damageSource, CallbackInfoReturnable<Boolean> cir) {
        PlayerEntity self = (PlayerEntity) (Object) this;
        if (self.hasStatusEffect(Capiche.METAL)) {
            cir.setReturnValue(true); // TODO: this is probably excessive
        }
    }

    @Inject(at = @At("HEAD"), method = "collideWithEntity")
    public void collideWithEntity(Entity entity, CallbackInfo ci) {
        PlayerEntity self = (PlayerEntity) (Object) this;
        if (self.hasStatusEffect(Capiche.METAL) && entity instanceof HostileEntity hostileEntity) {
            hostileEntity.damage(self.getDamageSources().playerAttack(self), 1000000); // TODO: this is definitely excessive
        }
    }

    @Inject(at = @At("HEAD"), method = "updateSwimming", cancellable = true)
    public void updateSwimming(CallbackInfo ci) {
        PlayerEntity self = (PlayerEntity) (Object) this;
        if (self.hasStatusEffect(Capiche.METAL)) {
            self.setSwimming(false);
            ci.cancel();
        }
    }
}
