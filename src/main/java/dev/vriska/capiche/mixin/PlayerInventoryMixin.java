package dev.vriska.capiche.mixin;

import dev.vriska.capiche.Capiche;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(PlayerInventory.class)
public class PlayerInventoryMixin {
    @Inject(at = @At("HEAD"), method = "insertStack(Lnet/minecraft/item/ItemStack;)Z", cancellable = true)
    private void insertStack(ItemStack stack, CallbackInfoReturnable<Boolean> cir) {
        if (stack.getItem() == Capiche.METAL_CAP) {
            PlayerInventory inv = (PlayerInventory) (Object) this;
            inv.player.addStatusEffect(new StatusEffectInstance(Capiche.METAL, 20 * 20));
            stack.setCount(0);
            cir.setReturnValue(true);
        }
    }
}
