package dev.vriska.capiche;

import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectCategory;

public class MetalStatusEffect extends StatusEffect {
    public MetalStatusEffect() {
        super(StatusEffectCategory.NEUTRAL, 0xA0A0A0);
    }
}