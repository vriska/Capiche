package dev.vriska.capiche;

import net.fabricmc.api.ModInitializer;

import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.minecraft.block.Block;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Capiche implements ModInitializer {
	// This logger is used to write text to the console and the log file.
	// It is considered best practice to use your mod id as the logger's name.
	// That way, it's clear which mod wrote info, warnings, and errors.
    public static final Logger LOGGER = LoggerFactory.getLogger("capiche");

	public static final Block GREEN_CAP_BLOCK = new GreenCapBlock(FabricBlockSettings.create().strength(3.0f).requiresTool().nonOpaque());
	public static final BlockEntityType<GreenCapBlockEntity> GREEN_CAP_BLOCK_ENTITY = Registry.register(
		Registries.BLOCK_ENTITY_TYPE,
		new Identifier("capiche", "green_cap_block_entity"),
		FabricBlockEntityTypeBuilder.create(GreenCapBlockEntity::new, GREEN_CAP_BLOCK).build()
	);

	public static final Item METAL_CAP = new Item(new FabricItemSettings());

	public static final StatusEffect METAL = new MetalStatusEffect();

	@Override
	public void onInitialize() {
		Registry.register(Registries.BLOCK, new Identifier("capiche", "green_cap_block"), GREEN_CAP_BLOCK);
		Registry.register(Registries.ITEM, new Identifier("capiche", "green_cap_block"), new BlockItem(GREEN_CAP_BLOCK, new FabricItemSettings()));
		Registry.register(Registries.ITEM, new Identifier("capiche", "metal_cap"), METAL_CAP);
		Registry.register(Registries.STATUS_EFFECT, new Identifier("capiche", "metal"), METAL);
	}
}